<?php
include"header1.php";
?>
      <div class="content-page">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
               <div class="card">
                  <div class="card-header d-flex justify-content-between">
                     <div class="header-title">
                        <h4 class="card-title">Reports</h4>
                     </div>
                  <div class="header-action">
                           <i  type="button" data-toggle="collapse" data-target="#datatable-1" aria-expanded="false" aria-controls="alert-1">
                             <a href="clubreport.php" class="btn btn-outline-primary mt-2 btn-with-icon"><i class="ri-user-line"></i>CLUB</a>
                           </i>
                        </div>

                        <div class="header-action">
                           <i  type="button" data-toggle="collapse" data-target="#datatable-1" aria-expanded="false" aria-controls="alert-1">
                             <a href="projectreport.php" class="btn btn-outline-primary mt-2 btn-with-icon"><i class="ri-user-line"></i>PROJECT</a>
                           </i>
                        </div>

                        <div class="header-action">
                           <i  type="button" data-toggle="collapse" data-target="#datatable-1" aria-expanded="false" aria-controls="alert-1">
                             <a href="meetingreport.php" class="btn btn-outline-primary mt-2 btn-with-icon"><i class="ri-user-line"></i>MEETING</a>
                           </i>
                        </div>

                        <div class="header-action">
                           <i  type="button" data-toggle="collapse" data-target="#datatable-1" aria-expanded="false" aria-controls="alert-1">
                             <a href="memberreport.php" class="btn btn-outline-primary mt-2 btn-with-icon"><i class="ri-user-line"></i>MEMBERS</a>
                           </i>
                        </div>


                  </div>
                  <div class="card-body">
                     <div>
                           <!-- <div class="card"><kbd class="bg-dark"><pre id="bootstrap-datatables" class="text-white"><code>

</code></pre></kbd></div> -->
                        </div>
                     <!-- <p>Images in Bootstrap are made responsive with <code>.img-fluid</code>. <code>max-width: 100%;</code> and <code>height: auto;</code> are applied to the image so that it scales with the parent element.</p> -->
                     <div class="table-responsive">
                        <table id="datatable" class="table data-table table-striped table-bordered" >
                           <thead>
                              <tr>
                                 <th>Name</th>
                                 <th>ID</th>
                                 <th>Email</th>
                                 <th>Phone</th>
                                 <th>President</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>updown</td>
                                 <td>rtrup123</td>
                                 <td>rtrupdown@gmail.com</td>
                                 <td>1234567890</td>
                                 <td>xxxx</td>
                                 <td><a href="add.php">EDIT</a> | <a href="add.php">DELETE</a></td>
                              </tr>
                           </tbody>
                           <tfoot>
                              <tr>
                                 <th>Name</th>
                                 <th>ID</th>
                                 <th>Email</th>
                                 <th>Phone</th>
                                 <th>President</th>
                                 <th>Action</th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
            <a href="dashboard.php" class="btn btn-danger">Back</a>
         </div>
      </div>
      </div>
    </div>
    <!-- Wrapper End-->

<?php
include"footer1.php";
?>