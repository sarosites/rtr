<?php
 include "header.php"
 ?>




<div class="page-title-area title-img-one">
<div class="title-shape">
<img src="assets/images/page-title/title-shape.jpg" alt="Title">
<img src="assets/images/banner/banner-shape2.png" alt="Title">
</div>
<div class="d-table">
<div class="d-table-cell">
<div class="container">
<div class="title-content">
<h2>About Us</h2>
<ul>
<li>
<a href="index.html">Home</a>
</li>
<li>
<span>About Us</span>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>


<div class="about-area pt-100 pb-70">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-6">
<div class="about-content">
<div class="section-title">
<span class="sub-title">About The Leader</span>
<h2>Meet The Leader Of The Future <span>Henry James Parker</span></h2>
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur numquam magnam, sed perspiciatis mollitia nesciunt amet, consequuntur error culpa nobis dolore asperiores eum non quasi</p>
</div>
<ul class="align-items-center">
<li>
<img src="assets/images/signature.png" alt="Signature">
</li>
<li>
<h3>Henry James</h3>
<span>Chairman, Pily</span>
</li>
</ul>
</div>
</div>
<div class="col-lg-6">
<div class="about-img">
<img src="assets/images/about-main.jpg" alt="About">
</div>
</div>
</div>
</div>
</div>


<section class="mission-area pt-100 pb-70">
<div class="mission-shape">
<img src="assets/images/mission/mission-shape.png" alt="Mission">
</div>
<div class="container">
<div class="section-title">
<span class="sub-title">Mission & Vision</span>
<h2>Our Mission & Vision</h2>
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur numquam magnam, sed perspiciatis mollitia nesciunt amet, consequuntur error culpa nobis dolore asperiores eum non quasi</p>
</div>
<div class="row">
<div class="col-sm-6 col-lg-4">
<div class="mission-item">
<img src="assets/images/mission/mission1.jpg" alt="Mission">
<div class="bottom">
<h3>
<a href="mission-details.html">Give People The Top Priority</a>
</h3>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry</p>
<a class="common-btn" href="mission-details.html">
Read More
<i class="icofont-long-arrow-right"></i>
</a>
</div>
</div>
</div>
<div class="col-sm-6 col-lg-4">
<div class="mission-item">
<img src="assets/images/mission/mission2.jpg" alt="Mission">
<div class="bottom">
<h3>
<a href="mission-details.html">Build A Strong Country</a>
</h3>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry</p>
<a class="common-btn" href="mission-details.html">
Read More
<i class="icofont-long-arrow-right"></i>
</a>
</div>
</div>
</div>
<div class="col-sm-6 offset-sm-3 offset-lg-0 col-lg-4">
<div class="mission-item">
<img src="assets/images/mission/mission3.jpg" alt="Mission">
<div class="bottom">
<h3>
<a href="mission-details.html">Achieve Peacefulness</a>
</h3>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry</p>
<a class="common-btn" href="mission-details.html">
Read More
<i class="icofont-long-arrow-right"></i>
</a>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="ideology-area pb-70">
<div class="container">
<div class="section-title">
<span class="sub-title">Our Ideology</span>
<h2>We Walk Through Our Ideology</h2>
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur numquam magnam, sed perspiciatis mollitia nesciunt amet, consequuntur error culpa nobis dolore asperiores eum non quasi</p>
</div>
<div class="row">
<div class="col-sm-6 col-lg-4">
<div class="ideology-item">
<img src="assets/images/ideology/ideology1.jpg" alt="Ideology">
<div class="bottom">
<h3>Build Strong Country</h3>
</div>
</div>
</div>
<div class="col-sm-6 col-lg-4">
<div class="ideology-item">
<img src="assets/images/ideology/ideology2.jpg" alt="Ideology">
<div class="bottom">
<h3>Fair Governance</h3>
</div>
</div>
</div>
<div class="col-sm-6 offset-sm-3 offset-lg-0 col-lg-4">
<div class="ideology-item">
<img src="assets/images/ideology/ideology3.jpg" alt="Ideology">
<div class="bottom">
<h3>Build Strong Community</h3>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="team-area two pb-100">
<div class="team-shape">
<img src="assets/images/donate-shape2.png" alt="Shape">
</div>
<div class="container">
<div class="section-title two">
<span class="sub-title">Volunteers</span>
<h2>Our Successful Volunteers</h2>
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur numquam magnam, sed perspiciatis mollitia nesciunt amet, consequuntur error culpa nobis dolore asperiores eum non quasi</p>
</div>
<div class="team-slider owl-theme owl-carousel">
<div class="team-item">
<div class="top">
<img src="assets/images/team/team1.jpg" alt="Team">
<ul>
<li>
<a href="#" target="_blank">
<i class="icofont-facebook"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-twitter"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-youtube-play"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-linkedin"></i>
</a>
</li>
</ul>
</div>
<div class="bottom">
<h3>Jane Ronan</h3>
<span>Campaign Influencer</span>
</div>
</div>
<div class="team-item">
<div class="top">
<img src="assets/images/team/team2.jpg" alt="Team">
<ul>
<li>
<a href="#" target="_blank">
<i class="icofont-facebook"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-twitter"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-youtube-play"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-linkedin"></i>
</a>
</li>
</ul>
</div>
<div class="bottom">
<h3>Tom Henry</h3>
<span>Donation Collector</span>
</div>
</div>
<div class="team-item">
<div class="top">
<img src="assets/images/team/team3.jpg" alt="Team">
<ul>
<li>
<a href="#" target="_blank">
<i class="icofont-facebook"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-twitter"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-youtube-play"></i>
</a>
</li>
<li>
 <a href="#" target="_blank">
<i class="icofont-linkedin"></i>
</a>
</li>
</ul>
</div>
<div class="bottom">
<h3>Erif Ertan</h3>
<span>Donation Collector</span>
</div>
</div>
<div class="team-item">
<div class="top">
<img src="assets/images/team/team4.jpg" alt="Team">
<ul>
<li>
<a href="#" target="_blank">
<i class="icofont-facebook"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-twitter"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-youtube-play"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-linkedin"></i>
</a>
</li>
</ul>
</div>
<div class="bottom">
<h3>Micheal Shon</h3>
<span>Donation Collector</span>
</div>
</div>
</div>
</div>
</section>


<div class="counter-area pt-100 pb-70">
<div class="container">
<div class="row">
<div class="col-sm-6 col-lg-3">
<div class="counter-item">
<img src="assets/images/counter-shape2.png" alt="Shape">
<h3>
<span class="odometer" data-count="3210">00</span>
</h3>
<p>Donors</p>
</div>
</div>
<div class="col-sm-6 col-lg-3">
<div class="counter-item">
<img src="assets/images/counter-shape2.png" alt="Shape">
<h3>
<span class="odometer" data-count="7980">00</span>
</h3>
<p>Volunteers</p>
</div>
</div>
<div class="col-sm-6 col-lg-3">
<div class="counter-item">
<img src="assets/images/counter-shape2.png" alt="Shape">
<h3>
<span class="odometer" data-count="101">00</span>
</h3>
<p>Missions</p>
</div>
</div>
<div class="col-sm-6 col-lg-3">
<div class="counter-item">
<img src="assets/images/counter-shape2.png" alt="Shape">
<h3>
<span class="odometer" data-count="825">00</span>
</h3>
<p>Projects</p>
</div>
</div>
</div>
</div>
</div>


<section class="testimonials-area ptb-100">
<div class="container">
<div class="section-title">
<span class="sub-title">Testimonials</span>
<h2>What People Say About Our Work</h2>
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur numquam magnam, sed perspiciatis mollitia nesciunt amet, consequuntur error culpa nobis dolore asperiores eum non quasi</p>
</div>
<div class="testimonials-slider owl-theme owl-carousel">
<div class="testimonials-item">
<div class="top">
<i class="icofont-quote-right"></i>
<p>Lorem ipsum dolor sit amet elit. Dolores dignissimos suscipit veritatis sunt accusamus optio ratione debitis temporibus deserunt cum doloremque impedit sit id, unde quia tenetur mollitia quis consequatu</p>
</div>
<ul class="content align-items-center">
<li>
<img src="assets/images/testimonials1.jpg" alt="Testimonials">
</li>
<li>
<h3>Victor James</h3>
<span>Country Director</span>
<ul class="rating">
<li>
<i class="icofont-star checked"></i>
</li>
<li>
<i class="icofont-star checked"></i>
</li>
<li>
<i class="icofont-star checked"></i>
</li>
<li>
<i class="icofont-star checked"></i>
</li>
<li>
<i class="icofont-star checked"></i>
</li>
</ul>
</li>
</ul>
</div>
<div class="testimonials-item">
<div class="top">
<i class="icofont-quote-right"></i>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution</p>
</div>
<ul class="content align-items-center">
<li>
<img src="assets/images/testimonials2.jpg" alt="Testimonials">
</li>
<li>
<h3>Tom Henry</h3>
<span>Manager</span>
<ul class="rating">
<li>
<i class="icofont-star checked"></i>
</li>
<li>
<i class="icofont-star checked"></i>
</li>
<li>
<i class="icofont-star checked"></i>
</li>
<li>
<i class="icofont-star checked"></i>
</li>
<li>
<i class="icofont-star checked"></i>
</li>
</ul>
</li>
 </ul>
</div>
<div class="testimonials-item">
<div class="top">
<i class="icofont-quote-right"></i>
<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable</p>
</div>
<ul class="content align-items-center">
<li>
<img src="assets/images/testimonials3.jpg" alt="Testimonials">
</li>
<li>
<h3>Micheal Shon</h3>
<span>Staff</span>
<ul class="rating">
<li>
<i class="icofont-star checked"></i>
</li>
<li>
<i class="icofont-star checked"></i>
</li>
<li>
<i class="icofont-star checked"></i>
</li>
<li>
<i class="icofont-star checked"></i>
</li>
<li>
<i class="icofont-star checked"></i>
</li>
</ul>
</li>
</ul>
</div>
</div>
</div>
</section>


<div class="subscribe-area pt-100 pb-70">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-6">
<div class="subscribe-content">
<h2>Stay Up-To-Date With Our Movements Get Subscribe!</h2>
</div>
</div>
<div class="col-lg-6">
<div class="subscribe-item">
<form class="newsletter-form" data-toggle="validator">
<input type="email" class="form-control" placeholder="Enter Your Email" name="EMAIL" required autocomplete="off">
<button class="btn common-btn" type="submit">
Subscribe Now
<i class="icofont-long-arrow-right"></i>
</button>
<div id="validator-newsletter" class="form-result"></div>
</form>
</div>
</div>
</div>
</div>
</div>






 <?php
 include "footer.php"
 ?>