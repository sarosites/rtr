<?php
 include "header.php"
 ?>



<div class="page-title-area title-img-one">
<div class="title-shape">
<img src="assets/images/page-title/title-shape.jpg" alt="Title">
<img src="assets/images/banner/banner-shape2.png" alt="Title">
</div>
<div class="d-table">
<div class="d-table-cell">
<div class="container">
<div class="title-content">
<h2>Event Details</h2>
<ul>
<li>
<a href="index.html">Home</a>
</li>
<li>
<span>Event Details</span>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>


<div class="event-details-area pt-100 pb-70">
<div class="container">
<div class="row">
<div class="col-lg-7">
<div class="details-img">
<img src="assets/images/events/event-details1.jpg" alt="Event">
<br>
<h2>Protest Against Of The Black Community</h2>
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat, voluptatibus eveniet natus porro autem laborum sint molestiae explicabo consectetur ad laboriosam nostrum aliquid ducimus. Vel et vitae nam aperiam nostrum</p>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy</p>
<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text</p>
<ul>
<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry</li>
<li>It is a long established fact that a reader will be distracted by the readable</li>
<li>Contrary to popular belief, Lorem Ipsum is not simply random text</li>
<li>There are many variations of passages of Lorem Ipsum available</li>
<li>All the Lorem Ipsum generators on the Internet tend to repeat predefined</li>
</ul>
</div>
</div>
<div class="col-lg-5">
<div class="common-details-content">
<h3>Event Details</h3>
<ul>
<li>
Location:
<a href="#">113 Pily, White House, New Jersey, USA</a>
</li>
<li>
Date:
<span>22 November 2020</span>
</li>
<li>
Time:
<span>11:30 AM</span>
</li>
<li>
Organizer:
<a href="#">Pily</a>
</li>
<li>
Phone:
<a href="tel:0113251432567">011-325-143-2567</a>
</li>
 <li>
Website:
<a href="#">www.pily.com</a>
</li>
</ul>
</div>
<br>




<h2>RELATED EVENTS</h2>
<br>
<div class="details-recent">
<div class="events-inner">
<ul class="align-items-center main-wrap">
<li>
<a href="event-details.html">
<img src="assets/images/events/events1.jpg" alt="Events">
</a>
</li>
<li>
<ul class="align-items-center link-wrap">
<li>
<span>12 Nov</span>
</li>
<li>
<h3>
<a href="event-details.html">Protest Against Of The Black Community</a>
</h3>
</li>
</ul>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit, modi officiis repudiandae reprehenderit</p>
</li>
</ul>
</div>
<div class="events-inner">
<ul class="align-items-center main-wrap">
<li>
<a href="event-details.html">
<img src="assets/images/events/events2.jpg" alt="Events">
</a>
</li>
<li>
<ul class="align-items-center link-wrap">
<li>
<span>13 Nov</span>
</li>
<li>
<h3>
<a href="event-details.html">Media Coverage Of The Protesting Event</a>
</h3>
</li>
</ul>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit, modi officiis repudiandae reprehenderit</p>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="subscribe-area pt-100 pb-70">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-6">
<div class="subscribe-content">
<h2>Stay Up-To-Date With Our Movements Get Subscribe!</h2>
</div>
</div>
<div class="col-lg-6">
<div class="subscribe-item">
<form class="newsletter-form" data-toggle="validator">
<input type="email" class="form-control" placeholder="Enter Your Email" name="EMAIL" required autocomplete="off">
<button class="btn common-btn" type="submit">
Subscribe Now
<i class="icofont-long-arrow-right"></i>
</button>
<div id="validator-newsletter" class="form-result"></div>
</form>
</div>
</div>
</div>
</div>
</div>




 <?php
 include "footer.php"
 ?>