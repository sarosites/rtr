<?php
 include "header.php"
 ?>




<div class="page-title-area title-img-one">
<div class="title-shape">
<img src="assets/images/page-title/title-shape.jpg" alt="Title">
<img src="assets/images/banner/banner-shape2.png" alt="Title">
</div>
<div class="d-table">
<div class="d-table-cell">
<div class="container">
<div class="title-content">
<h2>Contact</h2>
<ul>
<li>
<a href="index.html">Home</a>
</li>
<li>
<span>Contact</span>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>


<div class="contact-area pt-100 pb-70">
<div class="contact-shape">
<img src="assets/images/donate-shape2.png" alt="Shape">
</div>
<div class="container">
<div class="section-title">
<span class="sub-title">Contact</span>
<h2>Get In Touch</h2>
</div>
<div class="row">
<div class="col-lg-8">
<form id="contactForm">
<div class="row">
<div class="col-lg-6">
<div class="form-group">
<input type="text" name="name" id="name" class="form-control" placeholder="Name" required data-error="Please enter your name">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-lg-6">
<div class="form-group">
<input type="email" name="email" id="email" class="form-control" placeholder="Email" required data-error="Please enter your email">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-lg-6">
<div class="form-group">
<input type="text" name="phone_number" id="phone_number" placeholder="Phone" required data-error="Please enter your number" class="form-control">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-lg-6">
<div class="form-group">
<input type="text" name="msg_subject" id="msg_subject" class="form-control" placeholder="Subject" required data-error="Please enter your subject">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-lg-12">
<div class="form-group">
<textarea name="message" class="form-control" id="message" cols="30" rows="8" placeholder="Write message" required data-error="Write your message"></textarea>
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-lg-12">
<div class="form-check">
<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
<label class="form-check-label" for="defaultCheck1">
Accept <a href="terms-conditions.html">Terms of Services</a> and <a href="privacy-policy.html">Privacy Policy</a>
 </label>
</div>
</div>
<div class="col-lg-12">
<button type="submit" class="btn common-btn">
Send Message
</button>
<div id="msgSubmit" class="h3 text-center hidden"></div>
<div class="clearfix"></div>
</div>
</div>
</form>
</div>
<div class="col-lg-4">
<div class="contact-info">
<div class="inner">
<h3>Contact Info</h3>
<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Reprehenderit</p>
<ul>
<li>
<i class="icofont-location-pin"></i>
<a href="#">113 Pily, White House, New Jersey, USA</a>
</li>
<li>
<i class="icofont-ui-call"></i>
<a href="tel:+0015481592491">+001-548-159-2491</a>
<a href="tel:+0126584895236">+012-658-489-5236</a>
</li>
<li>
<i class="icofont-paper-plane"></i>
<a href="https://templates.hibootstrap.com/cdn-cgi/l/email-protection#3159545d5d5e7141585d481f525e5c"><span class="__cf_email__" data-cfemail="3c54595050537c4c555045125f5351">[email&#160;protected]</span></a>
<a href="https://templates.hibootstrap.com/cdn-cgi/l/email-protection#5a33343c351a2a33362374393537"><span class="__cf_email__" data-cfemail="f69f989099b6869f9a8fd895999b">[email&#160;protected]</span></a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="map-area pb-100">
<div class="container p-0">
<iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d59843174.53779284!2d62.17507173408573!3d23.728204508550373!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3663f18a24cbe857%3A0xa9416bfcd3a0f459!2sAsia!5e0!3m2!1sen!2sbd!4v1605092923871!5m2!1sen!2sbd" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
</div>






 <?php
 include "footer.php"
 ?>