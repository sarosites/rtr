<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from templates.hibootstrap.com/pily/default/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Mar 2021 06:33:07 GMT -->
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="assets/css/bootstrap.min.css">

<link rel="stylesheet" href="assets/css/icofont.min.css">

<link rel="stylesheet" href="assets/css/meanmenu.css">

<link rel="stylesheet" href="assets/css/animate.min.css">

<link rel="stylesheet" href="assets/css/odometer.min.css">

<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">

<link rel="stylesheet" href="assets/css/modal-video.min.css">

<link rel="stylesheet" href="assets/css/nice-select.min.css">

<link rel="stylesheet" href="assets/css/style.css">

<link rel="stylesheet" href="assets/css/responsive.css">
<title>Pily - Political HTML Template</title>
<link rel="icon" type="image/png" href="assets/images/favicon.png">
</head>
<body>

<div class="loader">
<div class="d-table">
<div class="d-table-cell">
<div class="sub-loader"></div>
</div>
</div>
</div>


<div class="header-area three">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-7">
<div class="left">
<span>Watch:</span>
<a href="#">
How America's two-party doom loop in driving division
<i class="icofont-long-arrow-right"></i>
</a>
</div>
</div>
<div class="col-lg-5">
<div class="right">
<ul class="social-icon">
<li>
<a href="#" target="_blank">
<i class="icofont-facebook"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-twitter"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-google-plus"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-linkedin"></i>
</a>
</li>
</ul>
<div class="nav-flag-dropdown">
<select>
<option>English</option>
<option>العربيّة</option>
<option>Deutsch</option>
<option>Português</option>
</select>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="navbar-area sticky-top">

<div class="mobile-nav">
<a href="index.html" class="logo">
<img src="assets/images/logo.png" alt="Logo">
</a>
</div>

<div class="main-nav two">
<div class="container">
<nav class="navbar navbar-expand-md navbar-light">
<a class="navbar-brand" href="index.html">
<img src="assets/images/logo-two.png" alt="Logo">
</a>
<div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
<ul class="navbar-nav">
<li class="nav-item">
<a href="about.html" class="nav-link">Home</a>
</li>
<li class="nav-item">
<a href="aboutus.php" class="nav-link">About Us</a>
</li>
<li class="nav-item">
<a href="about.html" class="nav-link">Management</a>
</li>
<li class="nav-item">
<a href="event.php" class="nav-link">Events</a>
</li>
<li class="nav-item">
<a href="blog.php" class="nav-link">Blog</a>
</li>
<li class="nav-item">
<a href="contact.php" class="nav-link">Contact</a>
</li>
</ul>
<div class="side-nav">
<a class="left" href="#">Join Us</a>
<a class="right common-btn two" href="donation.html">Donate</a>
</div>
</div>
</nav>
</div>
</div>
</div>