<?php
 include "header.php";
 include "db.php";
 ?>




<div class="page-title-area title-img-one">
<div class="title-shape">
<img src="assets/images/page-title/title-shape.jpg" alt="Title">
<img src="assets/images/banner/banner-shape2.png" alt="Title">
</div>
<div class="d-table">
<div class="d-table-cell">
<div class="container">
<div class="title-content">
<h2>Blog</h2>
<ul>
<li>
<a href="index.html">Home</a>
</li>
<li>
<span>Blog</span>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>


<div class="blog-area ptb-100">
<div class="container">
<div class="row align-items-center">
<div class="col-sm-6 col-lg-6">
<div class="blog-item">
	<?php
                    $sqlcat2=mysqli_query($conn,"SELECT * FROM blog WHERE del=0"); 
                    while($rowcat2=mysqli_fetch_array($sqlcat2))
                    {
                        ?>
<div class="top">
<a href="blogdetails.php?id=<?php echo $rowcat2['id'];?>">
<img src="../<?php echo $rowcat2['image'];?>" alt="Blog" width="100">
</a>
</div>
<div class="bottom">
<ul>
<li>
<i class="icofont-calendar"></i>
<span><?php echo $rowcat2['pdate'];?></span>
</li>
<!-- <li>
<i class="icofont-user-alt-4"></i>
<span>By:</span>
<a href="#">Admin</a>
</li> -->
</ul>
<h3>
<a href="blogdetails.php"><?php echo $rowcat2['title'];?></a>
</h3>
<p><?php echo $rowcat2['description'];?></p>
<a class="blog-btn" href="blogdetails.php">
Read More
<i class="icofont-long-arrow-right"></i>
</a>
</div>
</div>
</div>
<div class="col-sm-6 col-lg-6">
<div class="blog-item">
<div class="top">
<a href="blog-details.html">
<img src="assets/images/blog/blog3.jpg" alt="Blog">
</a>
</div>
<div class="bottom">
<ul>
<li>
<i class="icofont-calendar"></i>
<span>21 Oct, 2020</span>
</li>
<li>
<!-- <i class="icofont-user-alt-4"></i>
<span>By:</span>
<a href="#">Admin</a>
</li> -->
</ul>
<h3>
<a href="blog-details.html">Together We Can Achieve Anything By Work</a>
</h3>
<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat ut nisi impedit debitis maiores blanditiis eaque. Dicta minima et pariatur, nostrum ab impedit tenetur repellendus molestias</p>
<a class="blog-btn" href="blog-details.html">
Read More
 <i class="icofont-long-arrow-right"></i>
</a>
</div>
<?php
						}
						?>
</div>
</div>
<div class="col-sm-6 col-lg-6">
<div class="blog-item">
<div class="top">
<a href="blog-details.html">
<img src="assets/images/blog/blog4.jpg" alt="Blog">
</a>
</div>
<div class="bottom">
<ul>
<li>
<i class="icofont-calendar"></i>
<span>22 Oct, 2020</span>
</li>
<li>
<!-- <i class="icofont-user-alt-4"></i>
<span>By:</span>
<a href="#">Admin</a>
</li> -->
</ul>
<h3>
<a href="blog-details.html">How Communication Effects In Politics</a>
</h3>
<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat ut nisi impedit debitis maiores blanditiis eaque. Dicta minima et pariatur, nostrum ab impedit tenetur repellendus molestias</p>
<a class="blog-btn" href="blog-details.html">
Read More
<i class="icofont-long-arrow-right"></i>
</a>
</div>
</div>
</div>
<div class="col-sm-6 col-lg-6">
<div class="blog-item">
<div class="top">
<a href="blog-details.html">
<img src="assets/images/blog/blog5.jpg" alt="Blog">
</a>
</div>
<div class="bottom">
<ul>
<li>
<i class="icofont-calendar"></i>
<span>23 Oct, 2020</span>
</li>
<li>
<!-- <i class="icofont-user-alt-4"></i>
<span>By:</span>
<a href="#">Admin</a>
</li> -->
</ul>
<h3>
<a href="blog-details.html">How To Choose The Right Candidate To Vote</a>
</h3>
<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat ut nisi impedit debitis maiores blanditiis eaque. Dicta minima et pariatur, nostrum ab impedit tenetur repellendus molestias</p>
<a class="blog-btn" href="blog-details.html">
Read More
<i class="icofont-long-arrow-right"></i>
</a>
</div>
</div>
</div>
<div class="col-sm-6 col-lg-6">
<div class="blog-item">
<div class="top">
<a href="blog-details.html">
<img src="assets/images/blog/blog6.jpg" alt="Blog">
</a>
</div>
<div class="bottom">
 <ul>
<li>
<i class="icofont-calendar"></i>
<span>24 Oct, 2020</span>
</li>
<!-- <li>
<i class="icofont-user-alt-4"></i>
<span>By:</span>
<a href="#">Admin</a>
</li> -->
</ul>
<h3>
<a href="blog-details.html">How To Stand Against Any Violence</a>
</h3>
<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat ut nisi impedit debitis maiores blanditiis eaque. Dicta minima et pariatur, nostrum ab impedit tenetur repellendus molestias</p>
<a class="blog-btn" href="blog-details.html">
Read More
<i class="icofont-long-arrow-right"></i>
</a>
</div>
</div>
</div>
<div class="col-sm-6 col-lg-6">
<div class="blog-item">
<div class="top">
<a href="blog-details.html">
<img src="assets/images/blog/blog7.jpg" alt="Blog">
</a>
</div>
<div class="bottom">
<ul>
<li>
<i class="icofont-calendar"></i>
<span>25 Oct, 2020</span>
</li>
<!-- <li>
<i class="icofont-user-alt-4"></i>
<span>By:</span>
<a href="#">Admin</a>
</li> -->
</ul>
<h3>
<a href="blog-details.html">How To Become A good Citizen & Serve Country</a>
</h3>
<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat ut nisi impedit debitis maiores blanditiis eaque. Dicta minima et pariatur, nostrum ab impedit tenetur repellendus molestias</p>
<a class="blog-btn" href="blog-details.html">
Read More
<i class="icofont-long-arrow-right"></i>
</a>
</div>
</div>
</div>
</div>
<div class="pagination-area">
<ul>
<li>
<a href="#">Prev</a>
</li>
<li>
<a class="active" href="#">1</a>
</li>
<li>
<a href="#">2</a>
</li>
<li>
<a href="#">3</a>
</li>
<li>
<a href="#">Next</a>
</li>
</ul>
</div>
</div>
</div>






 <?php
 include "footer.php"
 ?>