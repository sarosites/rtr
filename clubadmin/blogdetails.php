<?php
 include "header.php";
 include "db.php";
 ?>





<div class="page-title-area title-img-one">
<div class="title-shape">
<img src="assets/images/page-title/title-shape.jpg" alt="Title">
<img src="assets/images/banner/banner-shape2.png" alt="Title">
</div>
<div class="d-table">
<div class="d-table-cell">
<div class="container">
<div class="title-content">
<h2>Blog Details</h2>
<ul>
<li>
<a href="index.php">Home</a>
</li>
<li>
<span>Blog Details</span>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>


<div class="blog-details-area pt-100 pb-70">
<div class="container">
<?php
        	$sql=mysqli_query($conn,"SELECT * FROM blog WHERE del=0 AND id='".$_GET['id']."'");

        	while ($row=mysqli_fetch_array($sql)) {
        		// print_r($row);exit;
        		?>
<div class="details-img">
<img src="../<?php echo $row['logo']; ?>" alt="Blog">
</div>
<div class="row">
<div class="col-lg-8">
<div class="details-item">
<div class="details-info">
<ul>
<li>
<i class="icofont-calendar"></i>
<span><?php echo $row['pdate']; ?></span>
</li>
<li>
<!-- <i class="icofont-user-alt-4"></i>
<span>By:</span>
<a href="#">Admin</a>
</li>
<li>
<i class="icofont-comment"></i>
<a href="#">02 Comments</a>
</li> -->
</ul>
<h2>We Can Achieve Anything By Heart</h2>
<p><?php echo $row['description']; ?></p>
<blockquote>
<i class="icofont-quote-right"></i>
Pily is excellent in their work and I really amazed to see how they managed everything throughout the year without any problem. They has an excellent sense of finance & economy.
<h3><span>By:</span> Phillips Ben</h3>
</blockquote>
<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>
</div>
 <div class="details-tag">
<div class="row align-items-center">
<div class="col-sm-6 col-lg-6">
<div class="left">
<ul>
<!-- <li>
<span>Tags:</span>
</li>
<li>
<a href="#">Politics</a>
</li>
<li>
<a href="#">Politician</a>
</li>
<li>
<a href="#">Country</a>
</li>
 --></ul>
</div>
</div>
<div class="col-sm-6 col-lg-6">
<div class="right">
<ul>
<li>
<span>Share:</span>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-facebook"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-twitter"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-linkedin"></i>
</a>
</li>
<li>
<a href="#" target="_blank">
<i class="icofont-instagram"></i>
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
<!-- <div class="details-comment">
<h3>Comments <span>(02)</span></h3>
<ul>
<li>
<img src="assets/images/blog/comment1.jpg" alt="Comment">
<h4>Tom Henry</h4>
<span>24 April, 2020</span>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, doloremque sapiente eaque rem vel tempora quasi placeat</p>
<a href="#">Reply</a>
</li>
<li>
<img src="assets/images/blog/comment2.jpg" alt="Comment">
<h4>Micheal Shon</h4>
<span>25 April, 2020</span>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, doloremque sapiente eaque rem vel tempora quasi placeat</p>
<a href="#">Reply</a>
</li>
</ul>
</div> -->


<!-- <div class="details-form">
<h3>Leave A Comment</h3>
<form>
<div class="row">
<div class="col-lg-6">
<div class="form-group">
<input type="text" class="form-control" placeholder="Name">
</div>
</div>
<div class="col-lg-6">
<div class="form-group">
<input type="email" class="form-control" placeholder="Email">
</div>
</div>
<div class="col-lg-12">
<div class="form-group">
<textarea id="your-comment" rows="8" class="form-control" placeholder="Comment"></textarea>
</div>
</div>
<div class="col-lg-12">
<button type="submit" class="btn common-btn">Post A Comment</button>
</div>
</div>
</form>
</div> -->
</div>
</div>
<div class="col-lg-4">
<div class="widget-area">
<!-- <div class="search widget-item">
<form>
<input type="text" class="form-control" placeholder="Search">
<button type="submit" class="btn">
<i class="icofont-search-1"></i>
</button>
</form>
</div> -->
<div class="news widget-item">
<h3>Recent News</h3>
<div class="inner">
<ul class="align-items-center">
<li>
<img src="assets/images/blog/news1.jpg" alt="Blog">
</li>
<li>
<i class="icofont-calendar"></i>
<span>25 Oct, 2020</span>
<h4>
<a href="#">How To Choose The Right Candidate To Vote</a>
</h4>
</li>
</ul>
</div>
<div class="inner">
<ul class="align-items-center">
<li>
<img src="assets/images/blog/news2.jpg" alt="Blog">
</li>
<li>
<i class="icofont-calendar"></i>
<span>26 Oct, 2020</span>
<h4>
<a href="#">How Communication Effects In Politics</a>
</h4>
</li>
</ul>
</div>
<div class="inner">
<ul class="align-items-center">
<li>
<img src="assets/images/blog/news3.jpg" alt="Blog">
</li>
<li>
<i class="icofont-calendar"></i>
<span>27 Oct, 2020</span>
<h4>
<a href="#">How To Become A Good Citizen & Serve Country</a>
</h4>
</li>
</ul>
</div>
</div>
<!-- <div class="tags widget-item">
<h3>Tags</h3>
<ul>
<li>
<a href="#">Politics</a>
</li>
<li>
<a href="#">Politician</a>
</li>
<li>
<a href="#">Country</a>
</li>
<li>
<a href="#">Love</a>
</li>
<li>
<a href="#">Patriotism</a>
</li>
<li>
<a href="#">Parliament</a>
</li>
<li>
<a href="#">Strategy</a>
</li>
<li>
<a href="#">Labour</a>
</li>
<li>
<a href="#">Election</a>
</li>
<li>
<a href="#">Vote</a>
</li>
</ul>
</div> -->
<!-- <div class="events-item">
<img src="assets/images/blog/blog-main1.jpg" alt="Blog">
<div class="bottom">
<h3>
<a href="event-details.html">Become A Proud Volunteer</a>
</h3>
<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sit ab iste</p>
<a class="common-btn" href="event-details.html">
Join Now
<i class="icofont-long-arrow-right"></i>
</a>
</div> -->
</div>
</div>
</div>
</div>
<?php
                            
                        }
                        ?>
</div>
</div>





 <?php
 include "footer.php"
 ?>