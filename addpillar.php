<?php
error_reporting(0);
session_start();
if(!isset($_SESSION['username']))
{
echo "<script>window.location.href='index.php'</script>";

}
else
{

include"header.php";

include("db.php");
// include("image_helper.php");

  
// $pillar=mysqli_query($conn,"SELECT * FROM pillar WHERE fld_delete=0");
// $num=mysqli_fetch_array($ret);
if(isset($_POST['pillarsub']))
{
  $name=$_POST['pillarname'];
  $project=$_POST['pillarproject'];
  $description=$_POST['pillardescription'];

 
  // upload code

$name1= $_FILES['file']['name'];

$tmp_name= $_FILES['file']['tmp_name'];

$position= strpos($name1, ".");

$fileextension= substr($name1, $position + 1);

$fileextension= strtolower($fileextension);


if (isset($name1)) {

$path= 'gallery/';
if (empty($name1))
{
echo "Please choose a file";
}
else if (!empty($name1)){
if (($fileextension !== "mp4") && ($fileextension !== "mkv") && ($fileextension !== "ogg") && ($fileextension !== "webm")&& ($fileextension !== "jpg")&& ($fileextension !== "jpeg")&& ($fileextension !== "png")&& ($fileextension !== "pdf"))
{
echo "The file extension must be .mp4, .ogg, or .webm in order to be uploaded";
}


else if (($fileextension == "mp4") || ($fileextension == "mkv")|| ($fileextension == "ogg") || ($fileextension == "webm")|| ($fileextension == "jpg")|| ($fileextension == "jpeg")|| ($fileextension == "png")|| ($fileextension == "pdf"))
{
if (move_uploaded_file($tmp_name, $path.$name1)) {
  $gallery=$path.$name1;

   $sql=mysqli_query($conn,"INSERT INTO pillar (name,project,description,gallery) VALUES ('".$name."','".$project."','".$description."','".$gallery."')");
echo 'Uploaded!';

}
}
}
}

  
  

    
    

  
  // end

                   

            
    echo  "<script>alert('Pillar Added Successfully')</script>";    
       
 
  echo "<script>window.location.href='pillar.php'</script>";
}

?>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header card">
<div class="card-block">
<h5 class="m-b-10">Pillar Management</h5>
<!-- <p class="text-muted m-b-10">HTML5 buttons provides the local file saving features</p>
 --><ul class="breadcrumb-title b-t-default p-t-10">
<li class="breadcrumb-item">
<a href="dashboard.php"> <i class="ti-home"></i> </a>
</li>
<li class="breadcrumb-item"><a href="pillar.php">Pillar</a>
</li>
<li class="breadcrumb-item"><a href="#!">Add Pillar</a>
  </li>
</ul>
</div>
</div>


<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header">
<h5>Add Pillar</h5>
<!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
</div>
<div class="card-block">
<!-- <h4 class="sub-title">Basic Inputs</h4> -->
<form action="" method="post"id="subform"  enctype="multipart/form-data">
<div class="form-group row">
<label class="col-sm-2 col-form-label">Name <b style="color: red">*</b></label>
<div class="col-sm-10">
<input type="text" class="form-control" name="pillarname" placeholder="Name" required="">
</div>
</div>
<div class="form-group row">
<label class="col-sm-2 col-form-label">Project</label>
<div class="col-sm-10">
<input type="text" class="form-control" name="pillarproject" placeholder="Project">
</div>
</div>
 <div class="form-group row">
<label class="col-sm-2 col-form-label">Description </label>
<div class="col-sm-10">
<textarea class="form-control"  name="pillardescription"   maxlength="1200"></textarea>
</div>
</div>
<div class="form-group row">
<label class="col-sm-2 col-form-label">Image</label>
<div class="col-sm-10">
<input type="file" class="form-control" name="file" size="100">
</div>
</div>
<br>
<div class="form-group row">
<div class="col-sm-4">
</div>
<div class="col-sm-4">
<input type="submit" class="form-control btn btn-info" name="pillarsub" value="Submit" style="border-radius: 12px;">
</div>
<div class="col-sm-4">
</div>
</div>
</form>

</div>
</div>
</div>


</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
  
  CKEDITOR.replace( 'pillardescription' );
        $("#subform").submit( function(e) {
            var messageLength = CKEDITOR.instances['pillardescription'].getData().replace(/<[^>]*>/gi, '').length;
            if( !messageLength ) {
                alert( 'Please enter a Description' );
                e.preventDefault();
            }
        });
</script>

<?php
include"footer.php";
}
?>