<?php
include"header1.php";
?>


      <div class="content-page">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <div class="d-flex align-items-center justify-content-between welcome-content">
                <div class="navbar-breadcrumb">
                    <h4 class="mb-0 font-weight-700">Welcome To Dashboard</h4>
                </div>
                <div class="">
                    <!-- <a class="btn btn-primary button-icon" target="_blank" href="#">View All</a> -->
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-danger">
                                    <svg class="svg-icon svg-danger" width="50" height="52" id="h-01"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
                                            d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
                                            d="M20.488 10H14V3.512A9.025 9.025 0 0120.488 9z" />
                                    </svg>
                                </div>

                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">0</h2>
                                    <p class="mb-0 text-danger">Total Clubs</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-success">
                                    <svg class="svg-icon svg-success mr-4" width="50" height="52" id="h-02"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
                                            d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z" />
                                    </svg>
                                </div>
                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">0</h2>
                                    <p class="mb-0 text-success">Total Project</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-primary">
                                    <svg class="svg-icon svg-blue mr-4" id="h-03" width="50" height="52"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                                    </svg>
                                </div>
                                <div class="mm-cart-text">
                                    <h2 class="font-weight-100">0</h2>
                                    <p class="mb-0 text-primary " style="font-size: 14px;">President/secretary</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-warning">
                                    <svg class="svg-icon svg-warning mr-4" id="h-04" width="50" height="52"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M7 12l3-3 3 3 4-4M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z" />
                                    </svg>
                                </div>
                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">0</h2>
                                    <p class="mb-0 text-warning">Report</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



<div class="col-lg-12">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-danger">
                                    <svg class="svg-icon svg-danger" width="50" height="52" id="h-01"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
                                            d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
                                            d="M20.488 10H14V3.512A9.025 9.025 0 0120.488 9z" />
                                    </svg>
                                </div>

                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">0</h2>
                                    <p class="mb-0 text-danger">District Events</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-success">
                                    <svg class="svg-icon svg-success mr-4" width="50" height="52" id="h-02"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
                                            d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z" />
                                    </svg>
                                </div>
                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">0</h2>
                                    <p class="mb-0 text-success"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-primary">
                                    <svg class="svg-icon svg-blue mr-4" id="h-03" width="50" height="52"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                                    </svg>
                                </div>
                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">0</h2>
                                    <p class="mb-0 text-primary">Customers</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-warning">
                                    <svg class="svg-icon svg-warning mr-4" id="h-04" width="50" height="52"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M7 12l3-3 3 3 4-4M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z" />
                                    </svg>
                                </div>
                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">200</h2>
                                    <p class="mb-0 text-warning">Visitors</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


         <div class="col-md-12">
            <div class="row">
            <div class="col-lg-6">
                <div class="card card-block card-stretch card-height">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">Avenue Report</h4>
                        </div>
                        
                        <div class="card-header-toolbar d-flex align-items-center">
                            <div class="dropdown">
                                <span class="dropdown-toggle dropdown-bg btn btn-outline-primary" id="dropdownMenuButton4"
                                    data-toggle="dropdown" aria-expanded="false">
                                    Monthly<i class="ri-arrow-down-s-line ml-1"></i>
                                </span>&nbsp;&nbsp; <a href="report.php" class="btn btn-danger">Report </a>

                                <div class="dropdown-menu dropdown-menu-right"
                                    aria-labelledby="dropdownMenuButton4" style="">
                                    <a class="dropdown-item" href="#">Daily</a>
                                    <a class="dropdown-item" href="#">Monthly</a>
                                    <a class="dropdown-item" href="#">Yearly</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="hospital-chart-02"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card card-block card-stretch card-height">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">Members</h4>
                        </div>
                        <div class="card-header-toolbar d-flex align-items-center">
                            <div class="dropdown">
                                <span class="dropdown-toggle dropdown-bg btn btn-outline-primary" id="dropdownMenuButton5"
                                    data-toggle="dropdown" aria-expanded="false">
                                    Monthly<i class="ri-arrow-down-s-line ml-1"></i>
                                </span>
                                <div class="dropdown-menu dropdown-menu-right"
                                    aria-labelledby="dropdownMenuButton5" style="">
                                    <a class="dropdown-item" href="#">Daily</a>
                                    <a class="dropdown-item" href="#">Monthly</a>
                                    <a class="dropdown-item" href="#">Yearly</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="hospital-chart-03"></div>
                    </div>
                </div>
            </div>
        </div>
        </div> 
        




        <!-- <div class="col-lg-6">
            <div class="card card-block card-stretch card-height">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">User's Progress</h4>
                    </div>
                    <div class="card-header-toolbar d-flex align-items-center">
                        <div class="dropdown">
                            <span class="dropdown-toggle dropdown-bg btn btn-outline-primary " id="dropdownMenuButton2"
                                data-toggle="dropdown">
                                Monthly<i class="ri-arrow-down-s-line ml-1"></i>
                            </span>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton2">
                                <a class="dropdown-item" href="#">Daily</a>
                                <a class="dropdown-item" href="#">Monthly</a>
                                <a class="dropdown-item" href="#">Yearly</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body align-items-center">
                    <div id="dash-chart-03"></div>
                    <div class="mt-1 p-0 list-inline text-center data-indicator d-flex justify-content-center">
                        <div class="">
                            <div class="d-flex align-items-center">
                                <i class="ri-checkbox-blank-circle-fill text-primary"></i>
                                <h2 class="line-height ml-2"><b>84</b></h2>
                            </div>
                            <p class="mb-0">Your Points</p>
                        </div>
                        <div class="ml-3">
                            <div class="d-flex align-items-center">
                                <i class="ri-checkbox-blank-circle-fill text-info"></i>
                                <h2 class="line-height ml-2"><b>64</b></h2>
                            </div>
                            <p class="mb-0">Average</p>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- <div class="col-lg-6">
            <div class="card card-block card-stretch card-height">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Upcoming Events</h4>
                    </div>
                    <div class="card-header-toolbar d-flex align-items-center">
                        <a href="#" class="btn btn-outline-primary view-more">View More</a>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="list-inline p-0 m-0">
                        <li class="d-flex align-items-center justify-content-between pb-3 border-bottom">
                            <div class="icon mm-icon-box event-icon bg-primary rounded-small font-size-18 mr-0">
                                <div class="date">23</div>
                                <div class="month">Oct</div>
                            </div>
                            <div class="event-info ml-3">
                                <h5>Samsing Mobile Flash Sale</h5>
                                <p class="mb-0 text-primary">08 : 30 Am</p>
                            </div>
                            <div class="d-flex align-items-center font-size-18">
                                <span class="mr-2"><i class="ri-edit-box-line"></i></span>
                                <span><i class="ri-delete-bin-line text-danger"></i></span>
                            </div>
                        </li>
                        <li class="d-flex align-items-center justify-content-between pb-3 pt-3 border-bottom">
                            <div class="icon mm-icon-box event-icon bg-info rounded-small font-size-18">
                                <div class="date">25</div>
                                <div class="month">Oct</div>
                            </div>
                            <div class="event-info ml-3">
                                <h5>Great Celebration Days</h5>
                                <p class="mb-0 text-primary">08 : 45 Am</p>
                            </div>
                            <div class="d-flex align-items-center font-size-18">
                                <span class="mr-2"><i class="ri-edit-box-line"></i></span>
                                <span><i class="ri-delete-bin-line text-danger"></i></span>
                            </div>
                        </li>
                        <li class="d-flex align-items-center justify-content-between pb-3 pt-3 border-bottom">
                            <div class="icon mm-icon-box event-icon bg-primary rounded-small font-size-18 mr-0">
                                <div class="date">23</div>
                                <div class="month">Oct</div>
                            </div>
                            <div class="event-info ml-3">
                                <h5>64GB Smart Phone Launch</h5>
                                <p class="mb-0 text-primary">08 : 30 Am</p>
                            </div>
                            <div class="d-flex align-items-center font-size-18">
                                <span class="mr-2"><i class="ri-edit-box-line"></i></span>
                                <span><i class="ri-delete-bin-line text-danger"></i></span>
                            </div>
                        </li>
                        <li class="d-flex align-items-center justify-content-between pt-3">
                            <div class="icon mm-icon-box event-icon bg-success rounded-small font-size-18">
                                <div class="date">28</div>
                                <div class="month">Oct</div>
                            </div>
                            <div class="event-info ml-3">
                                <h5>Personalize Gift Materials</h5>
                                <p class="mb-0 text-primary">09 : 00 Am</p>
                            </div>
                            <div class="d-flex align-items-center font-size-18">
                                <span class="mr-2"><i class="ri-edit-box-line"></i></span>
                                <span><i class="ri-delete-bin-line text-danger"></i></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div> -->
        <!-- <div class="col-lg-4">
            <div class="card card-block card-stretch card-height">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Top Performers</h4>
                    </div>
                    <div class="card-header-toolbar d-flex align-items-center">
                        <a href="#" class="btn btn-outline-primary view-more">View More</a>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="list-inline p-0 m-0">
                        <li class="media align-items-start">
                            <a href="JavaScript:Void(0);"><img src="../assets/images/user/1.jpg"
                                    class="img-fluid avatar-60 rounded-small" alt="image"></a>
                            <div class="media-body ml-3">
                                <div class="d-flex justify-content-between">
                                    <h5>Julia Met</h5>
                                    <span class="text-danger font-size-14">23 Jun 2020</span>
                                </div>
                                <span class="text-primary">New York</span>
                            </div>
                        </li>
                        <li class="media align-items-start mt-3">
                            <a href="JavaScript:Void(0);"><img src="../assets/images/user/5.jpg"
                                    class="img-fluid avatar-60 rounded-small" alt="image"></a>
                            <div class="media-body ml-3">
                                <div class="d-flex justify-content-between">
                                    <h5>Carolina Tens</h5>
                                    <span class="text-danger font-size-14">18 Dec 2020</span>
                                </div>
                                <span class="text-primary">California</span>
                            </div>
                        </li>
                        <li class="media align-items-start mt-3">
                            <a href="JavaScript:Void(0);"><img src="../assets/images/user/4.jpg"
                                    class="img-fluid avatar-60 rounded-small" alt="image"></a>
                            <div class="media-body ml-3">
                                <div class="d-flex justify-content-between">
                                    <h5>Anna Mull</h5>
                                    <span class="text-danger font-size-14">23 Aug 2020</span>
                                </div>
                                <span class="text-primary">Indiana</span>
                            </div>
                        </li>
                        <li class="media align-items-start mt-3">
                            <a href="JavaScript:Void(0);"><img src="../assets/images/user/5.jpg"
                                    class="img-fluid avatar-60 rounded-small" alt="image"></a>
                            <div class="media-body ml-3">
                                <div class="d-flex justify-content-between">
                                    <h5>Joan Watson</h5>
                                    <span class="text-danger font-size-14">31 Dec 2020</span>
                                </div>
                                <span class="text-primary">Chicago</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card card-block card-stretch card-height">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Social Media</h4>
                    </div>
                    <div class="card-header-toolbar d-flex align-items-center">
                        <a href="#" class="btn btn-outline-primary view-more">View More</a>
                    </div>
                </div>
                <div class="card-body">
                    <div id="dash-chart-02"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card card-block card-stretch card-height">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Products</h4>
                    </div>
                    <div class="card-header-toolbar d-flex align-items-center">
                        <a href="#" class="btn btn-outline-primary view-more">View More</a>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="list-inline p-0 m-0">
                        <li class="media align-items-center">
                            <a href="JavaScript:Void(0);"><img src="../assets/images/layouts/layout-1/prod-1.jpg"
                                    class="img-fluid avatar-60 rounded-small" alt="image"></a>
                            <div class="media-body ml-3">
                                <h5>Full Tees</h5>
                                <p class="mb-0 text-primary">100 Items</p>
                            </div>
                            <p class="mb-0  text-danger"><b>$ 12.8</b></p>
                        </li>
                        <li class="media align-items-center mt-3">
                            <a href="JavaScript:Void(0);"><img src="../assets/images/layouts/layout-1/prod-2.jpg"
                                    class="img-fluid avatar-60 rounded-small" alt="image"></a>
                            <div class="media-body ml-3">
                                <h5>Denim Jeans</h5>
                                <p class="mb-0 text-primary">92 Items</p>
                            </div>
                            <p class="mb-0  text-danger"><b>$ 15.6</b></p>
                        </li>
                        <li class="media align-items-center mt-3">
                            <a href="JavaScript:Void(0);"><img src="../assets/images/layouts/layout-1/prod-3.jpg"
                                    class="img-fluid avatar-60 rounded-small" alt="image"></a>
                            <div class="media-body ml-3">
                                <h5>Hip Hop Hat</h5>
                                <p class="mb-0 text-primary">50 Items</p>
                            </div>
                            <p class="mb-0  text-danger"><b>$ 18.5</b></p>
                        </li>
                        <li class="media align-items-center mt-3">
                            <a href="JavaScript:Void(0);"><img src="../assets/images/layouts/layout-1/prod-4.jpg"
                                    class="img-fluid avatar-60 rounded-small" alt="image"></a>
                            <div class="media-body ml-3">
                                <h5>Sports Shoes</h5>
                                <p class="mb-0 text-primary">95 Items</p>
                            </div>
                            <p class="mb-0  text-danger"><b>$ 11.0</b></p>
                        </li>
                    </ul>
                </div>
         -->    </div>
        </div>
    </div>
    <!-- Page end  -->
</div>
      </div>
    </div>
    <!-- Wrapper End-->
  


<?php
include"footer1.php";
?>