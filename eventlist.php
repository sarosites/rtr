<?php
include"header1.php";
include "db.php";

if(isset($_GET['uid']))
{

$id=$_GET['uid'];

$sql=mysqli_query($conn,"UPDATE event SET del=1 WHERE id='$id'");
echo "<script>window.location.href='eventlist.php'</script>";
}

?>

      <div class="content-page">
      <div class="container-fluid">
         <div class="col-md-12">
   <div class="row">
      <div class="col-lg-4 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-danger">
                                    <svg class="svg-icon svg-danger" width="50" height="52" id="h-01"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
                                            d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
                                            d="M20.488 10H14V3.512A9.025 9.025 0 0120.488 9z" />
                                    </svg>
                                </div>

                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">0</h2>
                                    <p class="mb-0 text-danger">On Going</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-success">
                                    <svg class="svg-icon svg-success mr-4" width="50" height="52" id="h-02"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"
                                            d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z" />
                                    </svg>
                                </div>
                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">0</h2>
                                    <p class="mb-0 text-success">Upcomming</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="mm-cart-image text-primary">
                                    <svg class="svg-icon svg-blue mr-4" id="h-03" width="50" height="52"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                                    </svg>
                                </div>
                                <div class="mm-cart-text">
                                    <h2 class="font-weight-700">0</h2>
                                    <p class="mb-0 text-primary">Completed</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
   </div>
</div>
         <div class="row">
            <div class="col-sm-12">
               <div class="card">
                  <div class="card-header d-flex justify-content-between">
                     <div class="header-title">
                        <h4 class="card-title">Event</h4>
                     </div>
                     <div class="header-title">
                         <label for="cars">Choose a Month & Year :</label>
                        <select>
                           <option>jan</option>
                           <option>Feb</option>
                           <option>March</option>
                        </select>
                        <select>
                           <option>2018</option>
                           <option>2019</option>
                           <option>2020</option>
                            <option>2021</option>
                        </select>
                         <a href="#" class="btn btn-outline-dark">Sumbit</a>
                     </div>
                  <div class="header-action">
                           <i  type="button" data-toggle="collapse" data-target="#datatable-1" aria-expanded="false" aria-controls="alert-1">
                             <a href="addevent.php" class="btn btn-dark"><i class="ri-user-line"></i>ADD EVENT</a>
                           </i>
                        </div>
                  </div>
                  <div class="card-body">
                     <div>
                           <!-- <div class="card"><kbd class="bg-dark"><pre id="bootstrap-datatables" class="text-white"><code>

</code></pre></kbd></div> -->
                        </div>
                     <!-- <p>Images in Bootstrap are made responsive with <code>.img-fluid</code>. <code>max-width: 100%;</code> and <code>height: auto;</code> are applied to the image so that it scales with the parent element.</p> -->
                     <div class="table-responsive">
                        <table id="datatable" class="table data-table table-striped table-bordered" >
                           <thead>
                              <tr>
                                 <th>Name</th>
                                 <th>ID</th>
                                 <th>Email</th>
                                 <th>Phone</th>
                                 
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                         <?php 
                         $ret=mysqli_query($conn,"SELECT * FROM event WHERE del=0");
                         $cnt=1;
                         while($row=mysqli_fetch_array($ret))
                        {
                        ?>
                              <tr>
                                 <td><?php echo $row['name'];?></td>
                                 <td><?php echo $row['cid'];?></td>
                                 <td><?php echo $row['email'];?></td>
                                 <td><?php echo $row['phone'];?></td>
                                 
                                 
                                 <td> <a href="eventview.php?uid=<?php echo $row['id'];?>"> 
                                     <button class="btn btn-primary btn-sm">View</button></a> <a href="eventlist.php?uid=<?php echo $row['id'];?>"> 
                                     
                                     <button class="btn btn-danger btn-sm" onClick="return confirm('Do you really want to delete');">Delete</button></a>

                                     <a href="editevent.php?uid=<?php echo $row['id'];?>"> 
                                     <button class="btn btn-primary btn-sm">Edit</button></a></td>
                              </tr>
                          <?php 
                              $cnt=$cnt+1; 
                           }
                           ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
            <a href="dashboard.php" class="btn btn-danger">Back</a>
         </div>
      </div>
      </div>
    </div>
    <!-- Wrapper End-->

<?php
include"footer1.php";
?>