<?php
 include "header2.php"
 ?>


<div class="main-banner">
<div class="container">
<div class="row">
<div class="col-lg-7 col-md-12">
<div class="main-banner-content">
<h1>Vote for democracy <span>& freedom</span></h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
<a href="#" class="btn btn-primary">Contact Us</a>
<a href="#" class="btn btn-secondary">Donate Now</a>
</div>
</div>
<div class="col-lg-5 col-md-12">
<div class="main-banner-img">
<img src="assets/img/main-banner.png" alt="image">
</div>
</div>
</div>
</div>
</div>


<section class="about-area ptb-100">
<div class="container">
<div class="row">
<div class="col-lg-6">
<div class="about-image">
<img src="assets/img/about-img.jpg" alt="image">
</div>
</div>
<div class="col-lg-6">
<div class="about-content">
<h2>About us.</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
<a href="#" class="btn btn-primary">Join Us</a>
</div>
</div>
</div>
<div class="row mt-100">
<div class="col-lg-4">
<div class="about-box">
<i class="icofont-diamond"></i>
<h3>Who we are</h3>
<p>Lorem ipsum dolor sit amet, adipiscing elit, eiusmod tempor incididunt abore dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
<a href="#" class="read-more">Read More</a>
</div>
</div>
<div class="col-lg-4">
<div class="about-box">
<i class="icofont-diamond"></i>
<h3>What we do</h3>
<p>Lorem ipsum dolor sit amet, adipiscing elit, eiusmod tempor incididunt abore dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
<a href="#" class="read-more">Read More</a>
</div>
</div>
<div class="col-lg-4">
<div class="about-box">
<i class="icofont-diamond"></i>
<h3>How we do</h3>
 <p>Lorem ipsum dolor sit amet, adipiscing elit, eiusmod tempor incididunt abore dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
<a href="#" class="read-more">Read More</a>
</div>
</div>
</div>
</div>
</section>


<section class="support-area ptb-100">
<div class="container">
<div class="row">
<div class="col-lg-6 col-md-12">
<div class="support-content">
<h3>Your political <span>support</span> make bigest help for big history</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
<a href="#" class="btn btn-secondary">Contact Us</a>
<a href="#" class="btn btn-primary">Donate Now</a>
</div>
</div>
<div class="col-lg-6 col-md-12">
<div class="support-image wow fadeInUp">
<img src="assets/img/shouting.png" alt="shouting">
</div>
</div>
</div>
</div>
</section>


<section class="mission-area ptb-100">
<div class="container">
<div class="section-title">
<h2>Our mission</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
</div>
<div class="row">
<div class="col-md-6 col-lg-6 col-xl-4">
<div class="single-mission">
<i class="icofont-education"></i>
<h3>Education development</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
</div>
</div>
<div class="col-md-6 col-lg-6 col-xl-4">
<div class="single-mission">
<i class="icofont-building"></i>
<h3>Medical development</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
</div>
</div>
<div class="col-md-6 col-lg-6 col-xl-4">
<div class="single-mission">
<i class="icofont-airplane-alt"></i>
<h3>Foreign policy development</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
</div>
</div>
<div class="col-md-6 col-lg-6 col-xl-4">
<div class="single-mission">
<i class="icofont-chart-pie"></i>
<h3>Economic development</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
</div>
</div>
<div class="col-md-6 col-lg-6 col-xl-4">
<div class="single-mission">
<i class="icofont-life-bouy"></i>
<h3>Election development</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
</div>
</div>
<div class="col-md-6 col-lg-6 col-xl-4">
<div class="single-mission">
<i class="icofont-heart-beat"></i>
<h3>Family development</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
</div>
</div>
</div>
</div>
</section>


<section class="funfacts-area">
<div class="map-image">
<img src="assets/img/map.png" alt="map">
</div>
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-6">
<div class="funFact top-line">
<h3><span class="count">25898</span>+</h3>
<p>Total members</p>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6">
<div class="funFact bottom-line">
<h3><span class="count">45787</span>+</h3>
<p>Total donations</p>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6">
<div class="funFact top-line">
<h3><span class="count">78452</span>+</h3>
<p>Total campaigns</p>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6">
<div class="funFact bottom-line">
<h3><span class="count">45898</span>+</h3>
<p>Our achievements</p>
</div>
</div>
</div>
</div>
</section>


<section class="upcoming-campaigns ptb-100">
<div class="container">
<div class="section-title">
<h2>Upcoming campaigns</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
</div>
<div class="row">
<div class="col-lg-6 col-md-12">
<div class="single-campaigns">
<div class="campaigns-img">
<img src="assets/img/campaign-img1.jpg" alt="campaign">
<div class="date">
<span>31</span>
Dec, 2018
</div>
</div>
<div class="campaigns-content">
<h3><a href="#">Purposeful aging town hall for Neighborhood Councils</a></h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
</div>
</div>
</div>
<div class="col-lg-6 col-md-12">
<div class="campaigns-box">
<div class="single-campaigns">
<div class="campaigns-content">
<div class="date">
<span>31</span>
Dec, 2018
</div>
<h3><a href="#">How to Publish Your Book in Today's Political Climate</a></h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
</div>
</div>
<div class="single-campaigns">
<div class="campaigns-content">
<div class="date">
<span>31</span>
Dec, 2018
</div>
<h3><a href="#">How should conservatives address perceptions of political bias online?</a></h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
</div>
</div>
<div class="single-campaigns">
<div class="campaigns-content">
<div class="date">
<span>31</span>
Dec, 2018
</div>
<h3><a href="#">RISE UP VIRGINIANS: Join us for a training on getting money out of politics</a></h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
</div>
</div>
<div class="single-campaigns">
<div class="campaigns-content">
<div class="date">
<span>31</span>
Dec, 2018
</div>
<h3><a href="#">China's Military and Geopolitical Rise and its Challenge to the US</a></h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
</div>
</div>
<div class="single-campaigns mb-0">
<div class="campaigns-content">
<div class="date">
<span>31</span>
Dec, 2018
</div>
<h3><a href="#">The Israel Lobby and American Policy 2019 Conference</a></h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="donate-area ptb-100">
<div class="container">
<div class="section-title">
<h2>Donate today</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
</div>
<div class="donate-price">
<ul>
<li><a href="#">$50</a></li>
<li><a href="#">$100</a></li>
<li><a href="#">$150</a></li>
<li><a href="#">$200</a></li>
<li><a href="#">$450</a></li>
</ul>
<a href="#" class="btn btn-primary">Donate Now</a>
</div>
</div>
</section>


<section class="history-area ptb-100">
<div class="container">
<div class="section-title">
<h2>Political history</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
</div>
<div class="row">
<div class="history-timeline">
<div class="single-history">
<div class="history-content left-side">
<h3>01</h3>
<h4>1750-1800</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
</div>
<div class="history-image">
<img src="assets/img/history-img1.jpg" alt="history">
</div>
</div>
<div class="single-history">
<div class="history-content right-side">
<h3>02</h3>
<h4>1800-1850</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
</div>
<div class="history-image">
<img src="assets/img/history-img2.jpg" alt="history">
</div>
</div>
<div class="single-history">
<div class="history-content left-side">
<h3>03</h3>
<h4>1850-1900</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
</div>
<div class="history-image">
<img src="assets/img/history-img3.jpg" alt="history">
</div>
</div>
 <div class="single-history">
<div class="history-content right-side">
<h3>04</h3>
<h4>1900-2000</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
</div>
<div class="history-image">
<img src="assets/img/history-img4.jpg" alt="history">
</div>
</div>
</div>
</div>
</div>
</section>


<section class="achievement-area ptb-100">
<div class="overlay"></div>
<div class="container-fluid">
<div class="row">
<div class="col-lg-6 col-md-4">
<div class="video text-center">
<a href="https://www.youtube.com/watch?v=bk7McNUjWgw" class="video-play-btn popup-video">
<i class="icofont-ui-play"></i>
<span class="ripple"></span>
<span class="ripple"></span>
<span class="ripple"></span>
</a>
</div>
</div>
<div class="col-lg-6 col-md-8">
<div class="achievement-content">
<div class="section-title">
<h2>Our achievement</h2>
</div>
<div class="row">
<div class="achievement-slides">
<div class="col-lg-12">
<div class="item">
<p>In this short period of six years, the advances of the Menzies period were consolidated. The Liberal governments moved ahead in the areas of traditional Liberal concern – health, education and welfare. The period is significant for a number of reasons, including foreign policy and defence initiatives, commitment to the aboriginal people and involvement with the rapidly changing world of technology, energy conservation and the environment.</p>
<h4>MacMahon Era (1966-1972)</h4>
</div>
</div>
<div class="col-lg-12">
<div class="item">
<p>The Liberal governments moved ahead in the areas of traditional Liberal concern – health, education and welfare. The period is significant for a number of reasons, including foreign policy and defence initiatives, commitment to the aboriginal people and involvement with the rapidly changing world of technology, energy conservation and the environment. In this short period of six years, the advances of the Menzies period were consolidated.</p>
<h4>John Era (1966-1972)</h4>
</div>
</div>
<div class="col-lg-12">
<div class="item">
<p>In this short period of six years, the advances of the Menzies period were consolidated. The Liberal governments moved ahead in the areas of traditional Liberal concern – health, education and welfare. The period is significant for a number of reasons, including foreign policy and defence initiatives, commitment to the aboriginal people and involvement with the rapidly changing world of technology, energy conservation and the environment.</p>
<h4>Steven Era (1966-1972)</h4>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="volunteer-area ptb-100">
<div class="container">
<div class="section-title">
<h2>Our volunteers</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
</div>
<div class="row">
<div class="team-slides">
<div class="col-lg-12 col-md-12">
<div class="single-team">
<img src="assets/img/team-img1.jpg" alt="team">
<div class="team-content">
<h4>Alex Maxwel</h4>
<span>CEO & President</span>
<ul>
<li><a href="#"><i class="icofont-share"></i></a>
<ul class="social-dropdown">
<li><a href="#"><i class="icofont-facebook"></i></a></li>
<li><a href="#"><i class="icofont-twitter"></i></a></li>
<li><a href="#"><i class="icofont-instagram"></i></a></li>
<li><a href="#"><i class="icofont-linkedin"></i></a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
<div class="col-lg-12 col-md-12">
<div class="single-team">
<img src="assets/img/team-img2.jpg" alt="team">
<div class="team-content">
<h4>Alex Maxwel</h4>
<span>CEO & President</span>
<ul>
<li><a href="#"><i class="icofont-share"></i></a>
<ul class="social-dropdown">
<li><a href="#"><i class="icofont-facebook"></i></a></li>
<li><a href="#"><i class="icofont-twitter"></i></a></li>
<li><a href="#"><i class="icofont-instagram"></i></a></li>
<li><a href="#"><i class="icofont-linkedin"></i></a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
<div class="col-lg-12 col-md-12">
<div class="single-team">
<img src="assets/img/team-img3.jpg" alt="team">
<div class="team-content">
<h4>Alex Maxwel</h4>
<span>CEO & President</span>
<ul>
<li><a href="#"><i class="icofont-share"></i></a>
<ul class="social-dropdown">
<li><a href="#"><i class="icofont-facebook"></i></a></li>
<li><a href="#"><i class="icofont-twitter"></i></a></li>
<li><a href="#"><i class="icofont-instagram"></i></a></li>
<li><a href="#"><i class="icofont-linkedin"></i></a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
<div class="col-lg-12 col-md-12">
<div class="single-team">
<img src="assets/img/team-img4.jpg" alt="team">
<div class="team-content">
<h4>Alex Maxwel</h4>
<span>CEO & President</span>
<ul>
<li><a href="#"><i class="icofont-share"></i></a>
<ul class="social-dropdown">
<li><a href="#"><i class="icofont-facebook"></i></a></li>
<li><a href="#"><i class="icofont-twitter"></i></a></li>
<li><a href="#"><i class="icofont-instagram"></i></a></li>
<li><a href="#"><i class="icofont-linkedin"></i></a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="testimonials-area ptb-100">
<div class="container">
<div class="row">
<div class="col-lg-6 col-md-12">
<h2>Feedback</h2>
<div class="feedback-slides">
<div class="item">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
<i class="icofont-quote-left"></i>
<div class="client-info">
<img src="assets/img/avator-1.png" alt="avator">
<h4>Ashlegh Gardner</h4>
<span>Founder & CEO, AWS Tech</span>
</div>
</div>
<div class="item">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
<i class="icofont-quote-left"></i>
<div class="client-info">
<img src="assets/img/avator-1.png" alt="avator">
<h4>Ashlegh Gardner</h4>
<span>Founder & CEO, AWS Tech</span>
</div>
</div>
<div class="item">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
<i class="icofont-quote-left"></i>
<div class="client-info">
<img src="assets/img/avator-1.png" alt="avator">
<h4>Ashlegh Gardner</h4>
<span>Founder & CEO, AWS Tech</span>
</div>
</div>
</div>
</div>
<div class="col-lg-6 col-md-12">
<div class="feedback-image wow fadeInUp">
<img src="assets/img/feedback-man.png" alt="feedback">
</div>
</div>
</div>
</div>
</section>


<section class="latest-news ptb-100">
<div class="container">
<div class="section-title">
<h2>Latest news</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
</div>
<div class="row">
<div class="news-slides">
<div class="col-lg-12 col-md-12">
<div class="single-blog-post">
<a href="#"><img src="assets/img/blog-img1.jpg" alt="blog"></a>
<div class="blog-content">
<h4><a href="#">New laws deliver a fairer and more sustainable GST system</a></h4>
<ul class="post-meta">
<li><a href="#"><i class="icofont-clock-time"></i> 23 Jan, 2019</a></li>
<li><a href="#"><i class="icofont-comment"></i> 15</a></li>
</ul>
<ul class="share-social">
<li><a href="#"><i class="icofont-share"></i></a>
<ul class="social-dropdown">
<li><a href="#"><i class="icofont-facebook"></i></a></li>
<li><a href="#"><i class="icofont-twitter"></i></a></li>
<li><a href="#"><i class="icofont-instagram"></i></a></li>
<li><a href="#"><i class="icofont-linkedin"></i></a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
 <div class="col-lg-12 col-md-12">
<div class="single-blog-post">
<a href="#"><img src="assets/img/blog-img2.jpg" alt="blog"></a>
<div class="blog-content">
<h4><a href="#">Fast tracking TAX relife for small and medium businesses</a></h4>
<ul class="post-meta">
<li><a href="#"><i class="icofont-clock-time"></i> 23 Jan, 2019</a></li>
<li><a href="#"><i class="icofont-comment"></i> 15</a></li>
</ul>
<ul class="share-social">
<li><a href="#"><i class="icofont-share"></i></a>
<ul class="social-dropdown">
<li><a href="#"><i class="icofont-facebook"></i></a></li>
<li><a href="#"><i class="icofont-twitter"></i></a></li>
<li><a href="#"><i class="icofont-instagram"></i></a></li>
<li><a href="#"><i class="icofont-linkedin"></i></a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
<div class="col-lg-12 col-md-12">
<div class="single-blog-post">
<a href="#"><img src="assets/img/blog-img3.jpg" alt="blog"></a>
<div class="blog-content">
<h4><a href="#">$50 million to provide life-saving support to cancer patients</a></h4>
<ul class="post-meta">
<li><a href="#"><i class="icofont-clock-time"></i> 23 Jan, 2019</a></li>
<li><a href="#"><i class="icofont-comment"></i> 15</a></li>
</ul>
<ul class="share-social">
<li><a href="#"><i class="icofont-share"></i></a>
<ul class="social-dropdown">
<li><a href="#"><i class="icofont-facebook"></i></a></li>
<li><a href="#"><i class="icofont-twitter"></i></a></li>
<li><a href="#"><i class="icofont-instagram"></i></a></li>
<li><a href="#"><i class="icofont-linkedin"></i></a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="newsletter-area ptb-100">
<div class="map-image">
<img src="assets/img/map.png" alt="map">
</div>
<div class="container">
<div class="newsletter">
<span>Get exclusive news & updates</span>
<h3>Our newsletter</h3>
<form>
<input type="text" class="form-control" placeholder="Enter your email address">
<button type="submit" class="subscribe-btn"><i class="icofont-envelope"></i> Subscribe Now</button>
</form>
</div>
</div>
</section>







 <?php
 include "footer2.php"
 ?>