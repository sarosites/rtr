<!doctype html>
<html lang="zxx">

<!-- Mirrored from templates.envytheme.com/kylehope/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 20 Mar 2021 06:57:52 GMT -->
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="assets/css/bootstrap.min.css">

<link rel="stylesheet" href="assets/css/animate.css">

<link rel="stylesheet" href="assets/css/icofont.min.css">

<link rel="stylesheet" href="assets/css/owl.carousel.css">

<link rel="stylesheet" href="assets/css/magnific-popup.css">

<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">

<link rel="stylesheet" href="assets/css/style.css">

<link rel="stylesheet" href="assets/css/responsive.css">
<title>KyleHope - Political HTML5 Template</title>
</head>
<body>

<div class="preloader-area">
<div class="sk-folding-cube">
<div class="sk-cube1 sk-cube"></div>
<div class="sk-cube2 sk-cube"></div>
<div class="sk-cube4 sk-cube"></div>
<div class="sk-cube3 sk-cube"></div>
</div>
</div>


<header class="header-area">

<div class="top-header">
<div class="container">
<div class="row">
<div class="col-lg-6 col-md-6">
<ul class="header-info">
<li><a href="#">Help</a></li>
<li><a href="#">Issues</a></li>
<li><a href="#">Volunter</a></li>
<li><a href="#">Policy</a></li>
</ul>
</div>
<div class="col-lg-6 col-md-6">
<ul class="social-link">
<li><a href="#"><i class="icofont-facebook"></i></a></li>
<li><a href="#"><i class="icofont-twitter"></i></a></li>
<li><a href="#"><i class="icofont-linkedin"></i></a></li>
<li><a href="#"><i class="icofont-skype"></i></a></li>
<li><a href="#"><i class="icofont-youtube-play"></i></a></li>
</ul>
</div>
</div>
</div>
</div>


<div class="middle-header">
<div class="container">
<div class="row">
<div class="col-lg-4 col-md-4">
<a href="index.html" class="logo">
<img src="assets/img/logo.png" alt="logo">
</a>
</div>
<div class="col-lg-8 col-md-8">
<ul>
<li>
<span class="icon">
<i class="icofont-phone"></i>
</span>
<span class="title">Give us a call</span>
<a href="#">+4 (8512) 745-4572</a>
</li>
<li>
<span class="icon">
<i class="icofont-envelope"></i>
</span>
<span class="title">Send us an email</span>
<a href="#"><span class="__cf_email__" data-cfemail="3950575f56797240555c7156495c175a5654">[email&#160;protected]</span></a>
</li>
 <li>
<div class="search-wrapper">
<div class="searchline-wrapper">
<input class="searchline" type="text" placeholder="Search...">
</div>
<button class="search-button">
<i class="icofont-search-1"></i>
<i class="icofont-close"></i>
</button>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>

</header>


<div class="main-menu-area header-sticky">
<div class="container">
<nav class="main-navbar-nav">
<div class="logo hide-min-992">
<a href="index.html"><img src="assets/img/logo.png" alt="logo"></a>
</div>
<ul class="main-nav">
<li><a href="#">Home</a>
<ul class="dropdown-menu">
<li class="active"><a href="index.html">Home demo One</a></li>
<li><a href="index-2.html">Home demo Two</a></li>
<li><a href="index-3.html">Home demo Three</a></li>
</ul>
</li>
<li><a href="about.html">About</a></li>
<li><a href="#">Events</a>
<ul class="dropdown-menu">
<li><a href="events.html">Events</a></li>
<li><a href="events-details.html">Events details</a></li>
</ul>
</li>
<li><a href="history.html">History</a></li>
<li><a href="#">Pages</a>
<ul class="dropdown-menu">
<li><a href="about.html">About</a></li>
<li><a href="history.html">History</a></li>
<li><a href="#">Volunteer</a>
<ul class="sub-menu">
<li><a href="volunteer.html">Volunteer</a></li>
<li><a href="volunteer-details.html">Volunteer details</a></li>
</ul>
</li>
<li><a href="gallery.html">Gallery</a></li>
<li><a href="#">Events</a>
<ul class="sub-menu">
<li><a href="events.html">Events</a></li>
<li><a href="events-details.html">Events details</a></li>
</ul>
</li>
<li><a href="#">News</a>
<ul class="sub-menu">
<li><a href="blog-right-sidebar.html">Blog right sidebar</a></li>
<li><a href="blog-left-sidebar.html">Blog left sidebar</a></li>
<li><a href="blog-details.html">Blog details</a></li>
</ul>
</li>
<li><a href="donate.html">Donate now</a></li>
<li><a href="become-a-volunteer.html">Become a volunteer</a></li>
<li><a href="faq.html">FAQ</a></li>
<li><a href="error.html">404 error</a></li>
</ul>
</li>
<li><a href="gallery.html">Gallery</a></li>
<li><a href="#">News</a>
<ul class="dropdown-menu">
<li><a href="blog-right-sidebar.html">Blog right sidebar</a></li>
<li><a href="blog-left-sidebar.html">Blog left sidebar</a></li>
<li><a href="blog-details.html">Blog details</a></li>
</ul>
</li>
<li><a href="contact.html">Contact</a></li>
 </ul>
<ul class="text-right donate-btn">
<li><a href="donate.html" class="btn btn-primary">Donate</a></li>
</ul>
</nav>
</div>
</div>





