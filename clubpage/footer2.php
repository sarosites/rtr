<footer class="footer-area">
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-6 ptb-100 bg">
<div class="footer-widget">
<div class="logo">
<a href="index.html"><img src="assets/img/footer-logo.png" alt="logo"></a>
</div>
<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p> -->
<ul class="social-links">
<li><a href="#"><i class="icofont-facebook"></i></a></li>
<li><a href="#"><i class="icofont-twitter"></i></a></li>
<li><a href="#"><i class="icofont-linkedin"></i></a></li>
<li><a href="#"><i class="icofont-skype"></i></a></li>
<li><a href="#"><i class="icofont-youtube-play"></i></a></li>
</ul>
</div>
</div>
<div class="col-lg-3 col-md-6 ptb-100">
<div class="footer-widget quick-links-widget">
<h3>Quick links</h3>
<ul class="quick-links">
<li><a href="#">Home</a></li>
<li><a href="#">About</a></li>
<li><a href="#">Gallery</a></li> 
<!-- <li><a href="#">Events</a></li> -->
<!-- <li><a href="#">Volunteers</a></li> -->
<!-- <li><a href="#">Campaigns</a></li> -->
<li><a href="#">Contact</a></li>
</ul>
</div>
</div>
<div class="col-lg-3 col-md-6 ptb-100">
<div class="footer-widget">
<h3>Recent post</h3>
<ul class="recent-post">
<li><a href="#">New laws deliver a fairer and more sustainable GST system</a></li>
<li><a href="#">Fast tracking TAX relife for small and medium businesses</a></li>
<!-- <li><a href="#">$50 million to provide life-saving support to cancer patients</a></li>
<li><a href="#">New laws deliver a fairer and more sustainable GST system</a></li> -->
</ul>
</div>
</div>
<div class="col-lg-3 col-md-6 ptb-100">
<div class="footer-widget">
<!-- <h3>Instagram</h3>
<ul class="instagram-post">
<li><a href="#"><img src="assets/img/blog-img1.jpg" alt=""></a></li>
<li><a href="#"><img src="assets/img/blog-img2.jpg" alt=""></a></li>
<li><a href="#"><img src="assets/img/blog-img3.jpg" alt=""></a></li>
<li><a href="#"><img src="assets/img/blog-img3.jpg" alt=""></a></li>
<li><a href="#"><img src="assets/img/blog-img2.jpg" alt=""></a></li>
<li><a href="#"><img src="assets/img/blog-img1.jpg" alt=""></a></li>
<li><a href="#"><img src="assets/img/blog-img1.jpg" alt=""></a></li>
<li><a href="#"><img src="assets/img/blog-img2.jpg" alt=""></a></li>
<li><a href="#"><img src="assets/img/blog-img3.jpg" alt=""></a></li>
</ul> -->
</div>
</div>
</div>
</div>
<div class="copyright-area">
<div class="container">
<div class="row">
<div class="col-lg-5 col-md-5">
<p>KyleHope<i class="icofont-copyright"></i> 2019. All Rights Reserved</p>
</div>
<div class="col-lg-7 col-md-7">
<ul>
<li><a href="#">Privacy Policy</a></li>
<li><a href="#">Terms & Conditions</a></li>
</ul>
</div>
</div>
</div>
</div>
</footer>

<div class="go-top"><i class="icofont-stylish-up"></i></div>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/jquery.min.js"></script>

<script src="assets/js/popper.min.js"></script>

<script src="assets/js/bootstrap.min.js"></script>

<script src="assets/js/owl.carousel.min.js"></script>

<script src="assets/js/jquery.magnific-popup.min.js"></script>

<script src="assets/js/jquery.mixitup.min.js"></script>

<script src="assets/js/jquery.meanmenu.js"></script>

<script src="assets/js/jquery.counterup.min.js"></script>

<script src="assets/js/waypoints.min.js"></script>

<script src="assets/js/wow.js"></script>

<script src="assets/js/main.js"></script>
</body>

<!-- Mirrored from templates.envytheme.com/kylehope/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 20 Mar 2021 06:58:06 GMT -->
</html>